﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private PlayerSettings m_PlayerSettings;

    [SerializeField] private LayerMask m_PlayerMasks;
    [SerializeField] private Player[] m_PlayerObjects;

    private Vector3[] m_PlayerStartPosition;
    private Health[] m_Health;

    [SerializeField] private FadeToBlack m_Fade;
    [SerializeField] private Text m_CountDownText;
    [SerializeField] private Canvas m_ControlsCanvas;

    private LevelObstacle[] m_Obstacles;


    /// Starts the game, keeps track of the gamePhases and sets all PlayerSettings.
	private IEnumerator Start() {

        m_PlayerSettings = new PlayerSettings();

        m_Obstacles = m_PlayerSettings.SetLevelElements();

        m_PlayerStartPosition = m_PlayerSettings.GetPlayerStartPositions(m_PlayerObjects);
        m_Health = m_PlayerSettings.GetPlayerHealth(m_PlayerObjects);

        while (true)
        {

            yield return StartCoroutine(MenuPhase());
            yield return StartCoroutine(BattlePhase());
            yield return StartCoroutine(GameOverPhase());

        }
		
	}


    /// Waits untill player presses "Play Button"
    private IEnumerator MenuPhase()
    {
        //Fade in level
        StartCoroutine(m_Fade.BeginFadeIn(1f));

        //Set all playerhealth to 100
        m_PlayerSettings.SetPlayerHealth(m_Health, 100f);

        SetInfoText("Press P to play!", 25);
        m_ControlsCanvas.enabled = true;

        while (!Input.GetKey(KeyCode.P))
        {

            yield return null;

        }

        m_ControlsCanvas.enabled = false;

        //Fade out slowly and run the countdown
        SetInfoText("", 215);
        StartCoroutine(m_Fade.BeginFadeOut(3f));
        yield return StartCoroutine(CountDown(m_CountDownText, 3, 1f));

    }

    /// Starts and ends all players and LevelObjects.
	private IEnumerator BattlePhase ()
    {

        StartCoroutine(m_Fade.BeginFadeIn(.25f));

        //Start all players
        foreach(Player player in m_PlayerObjects)
        {
            player.StartPlayer();
        }

        //Start all level objects
        foreach(LevelObstacle obstacle in m_Obstacles)
        {

            StartCoroutine(obstacle.StartObstacle(m_PlayerMasks));

        }

        //Wait untill one players has no more health
        do
        {
            yield return null;

        } while (m_Health[0].CurrentHealth > 0 && m_Health[1].CurrentHealth > 0);


        //Stop all players
        foreach (Player player in m_PlayerObjects)
        {
            player.StopPlayer();
        }

        //Stop all level obstacles
        foreach (LevelObstacle obstacle in m_Obstacles)
        {

            obstacle.StopObstacle();

        }

        yield return StartCoroutine(m_Fade.BeginFadeOut(1f));

    }

    /// Resets all players and LevelObjects
    private IEnumerator GameOverPhase()
    {

        StartCoroutine(m_Fade.BeginFadeIn(.25f));

        //Show infoText to press E to continue
        SetInfoText("Press E to return to menu!", 25);

        //Wait untill E is pressed.
        while (!Input.GetKey(KeyCode.E))
        {

            yield return null;

        }

        SetInfoText("", 25);

        yield return StartCoroutine(m_Fade.BeginFadeOut(1f));

        //Reset all players when E is pressed and the scene is faded out.
        m_PlayerSettings.SetPlayersAtStartPosition(m_PlayerObjects, m_PlayerStartPosition);

    }

    public void SetInfoText(string text, int fontSize)
    {

        m_CountDownText.fontSize = fontSize;
        m_CountDownText.text = text;

    }

    public IEnumerator CountDown(Text text, int countDownFrom, float delay)
    {

        for (int count = 0; count < countDownFrom; count++)
        {

            text.text = (countDownFrom - count).ToString();

            yield return new WaitForSeconds(delay);

        }

        text.text = "";

    }

}

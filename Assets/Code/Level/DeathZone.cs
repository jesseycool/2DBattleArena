﻿using UnityEngine;

public class DeathZone : LevelObstacle
{

    private float
        m_OffsetX = 40f,
        m_OffsetY = 20f;

    private Vector2 m_Start, m_End;

    public void Update()
    {

        Debug.DrawLine(new Vector2(transform.position.x - m_OffsetX, transform.position.y + m_OffsetY), new Vector2(transform.position.x + m_OffsetX, transform.position.y + m_OffsetY));

        Debug.DrawLine(new Vector2(transform.position.x + m_OffsetX, transform.position.y + m_OffsetY), new Vector2(transform.position.x + m_OffsetX, transform.position.y - m_OffsetY));

        Debug.DrawLine(new Vector2(transform.position.x + m_OffsetX, transform.position.y - m_OffsetY), new Vector2(transform.position.x - m_OffsetX, transform.position.y - m_OffsetY));

        Debug.DrawLine(new Vector2(transform.position.x - m_OffsetX, transform.position.y - m_OffsetY), new Vector2(transform.position.x - m_OffsetX, transform.position.y + m_OffsetY));

    }

    protected override void Initialize(LayerMask layerMask)
    {

        base.Initialize(layerMask);

        m_Damage = 100f;

        SetAreaBounds();

    }

    protected override Collider2D GetTarget
    {

        get { return Physics2D.OverlapArea(m_Start, m_End, m_LayerMask); }

    }

    private void SetAreaBounds()
    {

        m_Start = new Vector2(transform.position.x - m_OffsetX, transform.position.y - m_OffsetY);
        m_End = new Vector2(transform.position.x + m_OffsetX, transform.position.y + m_OffsetY);

    }

}

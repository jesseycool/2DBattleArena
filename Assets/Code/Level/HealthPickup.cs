﻿using System.Collections;
using UnityEngine;

public class HealthPickup : LevelObstacle {

    private float m_MovementSpeed = 1f,
                  m_MovementRange = 0.6f;

    protected override void Initialize(LayerMask layerMask)
    {

        base.Initialize(layerMask);

        m_Damage = -10f;

        StartCoroutine(MovePickup());

    }
     
    protected override Collider2D GetTarget
    {

        get { return Physics2D.OverlapCircle(transform.position, .5f, m_LayerMask); }

    }

    private IEnumerator MovePickup()
    {

        Vector3 pickupPosition = Vector3.zero;

        while (m_GameIsRunning)
        {

            pickupPosition.y = Mathf.Cos(Time.time * m_MovementSpeed) * m_MovementRange;

            transform.localPosition = pickupPosition;

            yield return null;

        }

    }

    protected override IEnumerator Resetting()
    {

        m_SpriteRenderer.color = new Color(m_SpriteRenderer.color.r, m_SpriteRenderer.color.g, m_SpriteRenderer.color.b, 0f);

        yield return new WaitForSeconds(Random.Range(1f, 3f));

        yield return StartCoroutine(m_SpriteAnimator.Flash(m_SpriteRenderer, 6, 0.25f));

    }


}

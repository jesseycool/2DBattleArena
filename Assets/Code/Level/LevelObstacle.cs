﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class LevelObstacle : MonoBehaviour
{

    protected LayerMask m_LayerMask;

    protected SpriteAnimator m_SpriteAnimator;
    protected SpriteRenderer m_SpriteRenderer;

    protected float m_Damage = 10f;

    protected bool m_GameIsRunning, m_AbleToDoDamage;

    protected virtual Collider2D GetTarget
    {

        get { return null; }
        
    }

    protected virtual void Initialize(LayerMask layerMask)
    {

        m_LayerMask = layerMask;
        m_GameIsRunning = true;

        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_SpriteAnimator = new SpriteAnimator();

    }

    public virtual IEnumerator StartObstacle(LayerMask layerMask)
    {

        Initialize(layerMask);

        while (m_GameIsRunning)
        {

            yield return StartCoroutine(Detecting());
            yield return StartCoroutine(Resetting());

        }

    }

    protected virtual IEnumerator Detecting()
    {

        m_AbleToDoDamage = true;

        Collider2D target = null;

        while (m_AbleToDoDamage)
        {

            target = GetTarget;

            if (target != null)
            {

                target.SendMessage("Damage", m_Damage);
                m_AbleToDoDamage = false;

            }

            yield return null;

        }

    }

    protected virtual IEnumerator Resetting()
    {

        yield return StartCoroutine(m_SpriteAnimator.Flash(m_SpriteRenderer, 6, 0.25f));

    }

    public virtual void StopObstacle()
    {

        m_GameIsRunning = false;

    }

}

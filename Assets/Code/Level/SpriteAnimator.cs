﻿using System.Collections;
using UnityEngine;

public class SpriteAnimator
{

    public IEnumerator Flash(SpriteRenderer sprite, int numberOfTimes, float delay)
    {

        for (int loop = 0; loop < numberOfTimes; loop++)
        {

            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0.25f);

            yield return new WaitForSeconds(delay);

            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1f);

            yield return new WaitForSeconds(delay);

        }

    }

}

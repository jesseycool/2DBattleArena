﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class Projectile : MonoBehaviour
{

    [SerializeField]
    private LayerMask m_LayerMask;

    private int m_AmountOfRays = 2;
    private float m_RaySpacing;

    private Vector2 m_Velocity;
    private float m_Gravity = -9.8f;

    private CircleCollider2D m_Collider;
    private Bounds m_Bounds;

    private Vector2 m_Centre;

    private void Start()
    {

        m_Collider = GetComponent<CircleCollider2D>();
        Invoke("Destroy", 5f);

    }

    public void SetVelocity(Vector3 newVelocity)
    {

        m_Velocity = newVelocity;

    }

    private void Update()
    {

        m_Velocity.y += m_Gravity * Time.deltaTime;

        MoveProjectile(m_Velocity * Time.deltaTime);

    }

    private void MoveProjectile(Vector2 velocity)
    {

        UpdateRaycastStart();

        CollidionDetection(ref velocity);

        transform.Translate(velocity);

    }

    private void CollidionDetection(ref Vector2 velocity)
    {

        float rayLength = velocity.magnitude;
        Vector2 direction = velocity.normalized;

        for (int ray = 0; ray < m_AmountOfRays; ray++)
        {

            RaycastHit2D rayCastHit = Physics2D.Raycast(m_Centre + Vector2.up * ray * m_RaySpacing, direction, m_Bounds.size.y / 2, m_LayerMask);

            Debug.DrawRay(m_Centre + Vector2.up * ray * m_RaySpacing, direction * (m_Bounds.size.y / 2), Color.red);


            if (rayCastHit)
            {

                //Todo: Change this to a less hard coded way!!
                //If you changed layers check if these are still correct: 
                if(rayCastHit.collider.gameObject.layer == 8 || rayCastHit.collider.gameObject.layer == 9)
                {
                    rayCastHit.collider.SendMessage("Damage", 5f);
                }
                
                Destroy();
                break;
            }

        }

    }

    private void UpdateRaycastStart()
    {

        m_Bounds = m_Collider.bounds;

        m_Centre = new Vector2(m_Bounds.center.x, m_Bounds.min.y);

        m_RaySpacing = m_Bounds.size.x / (m_AmountOfRays - 1);

    }

    private void Destroy()
    {

        Destroy(gameObject);

    }

}

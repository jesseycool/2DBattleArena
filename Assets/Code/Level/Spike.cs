﻿using UnityEngine;

public class Spike : LevelObstacle
{

    private float
        m_OffsetX = 0.5f,
        m_OffsetY = 0.4f;

    private Vector2 m_Start, m_End;


    protected override Collider2D GetTarget
    {

        get { return Physics2D.OverlapArea(m_Start, m_End, m_LayerMask); }

    }

    protected override void Initialize(LayerMask layerMask)
    {

        base.Initialize(layerMask);

        m_Damage = 15f;

        SetAreaBounds();

    }

    private void SetAreaBounds()
    {

        m_Start = new Vector2(transform.position.x - m_OffsetX, transform.position.y - m_OffsetY);
        m_End = new Vector2(transform.position.x + m_OffsetX, transform.position.y + m_OffsetY);

    }

}

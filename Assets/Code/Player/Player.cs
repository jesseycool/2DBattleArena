﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Combat))]
public class Player : MonoBehaviour
{

    private bool m_GameIsRunning;

    private BoxCollider2D m_Collider;
    private Combat m_Combat;

    [SerializeField] private string m_PlayerName = "Player01";                              //String used to determine what player to match with the correct (controller) input.
    [SerializeField] private LayerMask m_LayerMask;                                         //Layermask used for Collision
    [SerializeField] private float m_JumpHeight, m_JumpTime, m_MoveSpeed;                   //General movement variables
    [SerializeField] private int m_RayCount = 4;                                            //Amount of rays to cast, more rays is more accurate collision

    [SerializeField] private Image m_MeleeReloadImage, m_RangedReloadImage;                 //Radial images used to indicate reaload time
    [SerializeField] private float m_MeleeReloadTime = 0.6f, m_RangedReloadTime = 1f;       //Attack reload times
    private float m_MeleeTime, m_RangedTime;

    private float m_JumpVelocity, m_Gravity;
    private Vector2 m_Velocity;

    private Bounds m_Bounds;

    private int m_LastDirection;

    private CollisionDetection m_CollisionDetection;

    private void Start()
    {

        SetComponents();

        SetJumpHeight();

    }

    //Starts the player
    public void StartPlayer()
    {

        m_GameIsRunning = true;

        StartCoroutine(MovePlayer());

    }

    //All the playermovement is controlled here, stops when game is not in BattlePhase (See GameManager)
    private IEnumerator MovePlayer()
    {

        while (m_GameIsRunning)
        {

            MovementInput();
            CombatInput();
            Timer();

            m_LastDirection = GetCurrentDirection();

            m_Velocity.y += m_Gravity * Time.deltaTime;

            m_CollisionDetection.Movement(m_Velocity);

            yield return null;

        }

    }

    private void MovementInput()
    {

        if (m_CollisionDetection.CollisionFeedback.Above || m_CollisionDetection.CollisionFeedback.Below)
        {

            m_Velocity.y = 0f;

        }

        if (Input.GetButtonDown("Jump_" + m_PlayerName) && m_CollisionDetection.CollisionFeedback.Below)
        {

            m_Velocity.y = m_JumpVelocity;

        }

        m_Velocity.x = Input.GetAxisRaw("Horizontal_" + m_PlayerName) * m_MoveSpeed * Time.deltaTime;

     }

    private void CombatInput()
    {

        //Controller trigger L
        if (Input.GetAxisRaw("TriggerInput_" + m_PlayerName) < 0 && m_MeleeTime >= m_MeleeReloadTime)
        {

            MeleeCombat();

        }

        //Controller trigger R
        if (Input.GetAxisRaw("TriggerInput_" + m_PlayerName) > 0 && m_RangedTime >= m_RangedReloadTime)
        {

            RangedCombat();

        }

        //Keyboard input (See Edit > Project > InputManager)
        if (Input.GetButton("Melee_" + m_PlayerName) && m_MeleeTime >= m_MeleeReloadTime)
        {

            MeleeCombat();

        }

        //Keyboard input (See Edit > Project > InputManager)
        if (Input.GetButton("Ranged_" + m_PlayerName) && m_RangedTime >= m_RangedReloadTime)
        {

            RangedCombat();

        }

    }

    private void MeleeCombat()
    {

        m_MeleeTime = 0;

        m_Combat.MeleeCombat(CombatOrigin(m_LastDirection), m_LastDirection);

    }

    private void RangedCombat()
    {

        m_RangedTime = 0;
        m_Combat.RangedCombat();

    }

    void Timer()
    {

        if (m_MeleeTime < m_MeleeReloadTime)
        {

            m_MeleeTime += Time.deltaTime;
            m_MeleeReloadImage.fillAmount = m_MeleeTime / m_MeleeReloadTime;

        }

        if (m_RangedTime < m_RangedReloadTime)
        {

            m_RangedTime += Time.deltaTime;
            m_RangedReloadImage.fillAmount = m_RangedTime / m_RangedReloadTime;

        }

    }

    private void SetComponents()
    {

        m_Collider = GetComponent<BoxCollider2D>();
        m_Combat = GetComponent<Combat>();

        m_CollisionDetection = new CollisionDetection(m_LayerMask, transform, m_RayCount, m_Collider);

    }

    private void SetJumpHeight()
    {

        m_Gravity = -(2 * (m_JumpHeight / 10)) / Mathf.Pow(m_JumpTime, 2);
        m_JumpVelocity = Mathf.Abs(m_Gravity) * m_JumpTime;

    }

    private int GetCurrentDirection()
    {

        return m_Velocity.x < 0 ? -1 :
               m_Velocity.x > 0 ? 1 :
               m_LastDirection;

    }

    private Vector2 CombatOrigin(int direction)
    {

        m_Bounds = m_Collider.bounds;

        return direction == -1 ? new Vector2(m_Bounds.min.x, m_Bounds.center.y) : new Vector2(m_Bounds.max.x, m_Bounds.center.y);

    }

    //Stops the player
    public void StopPlayer()
    {

        m_GameIsRunning = false;

    }

}


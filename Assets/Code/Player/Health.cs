﻿using UnityEngine.UI;
using UnityEngine;

public class Health : MonoBehaviour
{

    [SerializeField] private Image m_HealthBar;
    [SerializeField] private Color m_StartColor;

    private float m_Health = 100f;

    public float CurrentHealth
    {

        get { return m_Health; }
        set
        {

            m_Health = value;
            SetHealthBarColor();

        }

    }

    public void Damage(float damage)
    {

        m_Health -= damage;

        if (m_Health > 100) m_Health = 100;

        SetHealthBarColor();

    }

    public void SetHealthBarColor()
    {

        m_HealthBar.color = m_StartColor * (m_Health / 100f);

    }

}

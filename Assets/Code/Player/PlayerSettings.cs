﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSettings : MonoBehaviour {

    public Health[] GetPlayerHealth(Player[] playerObjects)
    {

        Health[] playerHealth = new Health[playerObjects.Length];

        for (int i = 0; i < playerObjects.Length; i++)
        {

            playerHealth[i] = playerObjects[i].GetComponent<Health>();

        }

        return playerHealth;

    }

    public void SetPlayerHealth(Health[] playerHealth, float amount)
    {

        for (int i = 0; i < playerHealth.Length; i++)
        {

            playerHealth[i].CurrentHealth = amount;

        }

    }

    public Vector3[] GetPlayerStartPositions(Player[] playerObjects)
    {

        Vector3[] StartPosition = new Vector3[playerObjects.Length];

        for (int i = 0; i < playerObjects.Length; i++)
        {

            StartPosition[i] = playerObjects[i].transform.position;

        }

        return StartPosition;

    }

    public void SetPlayersAtStartPosition(Player[] playerObjects, Vector3[] playerStartPositions)
    {

        for (int i = 0; i < playerObjects.Length; i++)
        {

            playerObjects[i].transform.position = playerStartPositions[i];

        }

    }

    public LevelObstacle[] SetLevelElements()
    {

        return FindObjectsOfType(typeof(LevelObstacle)) as LevelObstacle[];

    }
}

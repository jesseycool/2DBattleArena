﻿using UnityEngine;
using System.Collections;

public class Combat : MonoBehaviour {

    [SerializeField] private LayerMask m_LayerMask;                     //Melee attack layermask, set this for the other player
    [SerializeField] private float m_ProjectileSpeed = 25f;             //Speed at whitch the projectile moves
    [SerializeField] private Transform m_Projectile, m_Crosshairs;      //Set the prefab and the crosshairs in the inspector
    [SerializeField] private ParticleSystem m_ParticleSystem;

    private ParticleSystem.EmissionModule m_EmissionModule;
    private ParticleSystem.VelocityOverLifetimeModule m_VelocityModule;

    private Vector3 m_Direction;

    public void MeleeCombat(Vector2 origin, float direction)
    {

        RaycastHit2D rayCastHit = Physics2D.Raycast(origin, Vector2.right * direction, 1f, m_LayerMask);
        Debug.DrawRay(origin, Vector2.right * direction);

        

        if (rayCastHit)
        {

            rayCastHit.collider.SendMessage("Damage", 10f);
            StartCoroutine(ParticleOneShot(direction));

        }

    }

    private IEnumerator ParticleOneShot(float direction)
    {

         
        m_VelocityModule = m_ParticleSystem.velocityOverLifetime;
        m_EmissionModule = m_ParticleSystem.emission;

        m_VelocityModule.x = 5 * direction;

        m_EmissionModule.rateOverTime = 100;

        yield return new WaitForSeconds(.15f);

        m_EmissionModule.rateOverTime = 0;

    }

    void Update()
    {
        //Controller (Untested):
        //Vector3 aimDirection = (new Vector3(Input.GetAxisRaw("Horizontal_Aim_" + player), -Input.GetAxisRaw("Vertical_Aim_" + player), 0).normalized * 2) + transform.position;
        //Vector3 m_Direction = (aimDirection - transform.position).normalized;
       
        //Mouse:
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 direciton = (mousePosition - transform.position);

        m_Direction = new Vector3(direciton.x, direciton.y, 0).normalized;

        m_Crosshairs.localPosition = m_Direction;

    }

    public void RangedCombat()
    {

        Transform newProjectile = Instantiate(m_Projectile);

        newProjectile.position = transform.position + m_Direction * 2f;
        newProjectile.GetComponent<Projectile>().SetVelocity(m_Direction * m_ProjectileSpeed);

    }

}
